local mod = {}
local nvim = vim.api

mod.m_buffer = nil

mod.get_or_create = function()
    if mod.m_buffer
        then return mod.m_buffer
        else do
            print "Creating"
            nvim.nvim_command(":vs | enew ")
            local buf = nvim.nvim_call_function('bufnr', {'%'})
            mod.m_buffer = buf

            nvim.nvim_buf_set_option(buf, "buftype", "nofile")
            nvim.nvim_buf_set_option(buf, "bufhidden", "unload")
            nvim.nvim_buf_set_option(buf, "undolevels", -1)
            nvim.nvim_buf_set_option(buf, "swapfile", false)
            nvim.nvim_buf_set_name(buf, "Proof State")

            print (string.format ("Done %d", buf))

            return buf
        end
    end
end

mod.setup_process = function()
    -- painful callback into here from lua
    local job = nvim.nvim_call_function("StartCoq", {{"coqtop", "-emacs"}});
    mod.m_job = job
    nvim.nvim_out_write(string.format ("Process %d\n", job))
end

local function split(s, delimiter)
    local result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end

local function process_err(str)
    nvim.nvim_out_write(str)
    local line, ctxt1, _state_id, ctxt2, response = string.match(str,
        '(.*)<prompt>%s*(.-)%s<%s*(.-)%s*|(.-)|%s*(.-)%s*<%s*</prompt>')
    ctxt1 = ctxt1 or "Coq"
    response = line .. "<" .. str
    nvim.nvim_buf_set_lines(mod.m_buffer, 20, 90, false,
    {ctxt1 .. ": " .. response})
end

mod.on_coq = function(dict)
    local inspect = require('inspect')
    --nvim.nvim_err_writeln(inspect(dict))
    if mod.m_buffer then
        if dict.event == "stdout"
        then
            for i = #dict.data + 1, 40 do
                dict.data[i] = ''
            end
            nvim.nvim_buf_set_lines(mod.m_buffer, 1, 90, false,
                dict.data)
        elseif dict.event == "stderr"
        then
            process_err(dict.data[#dict.data])
        end
    end
end

mod.command = function(command)
    local cmd = command or "Compute true."
    nvim.nvim_buf_set_lines(mod.m_buffer, 0, 1, false, {cmd})
    if mod.m_job and mod.m_job > 0 then
        nvim.nvim_call_function("chansend",
            {mod.m_job, cmd .. "\n\n"})
    else
        nvim.nvim_stderr_writeln("No coq instance running")
    end
end

mod.load_fresh_to_cursor = function()
    local win = nvim.nvim_get_current_win()
    local buf = nvim.nvim_win_get_buf(win)
    local pos = nvim.nvim_win_get_cursor(win)
    nvim.nvim_err_writeln(pos[1] .. " " .. pos[2])
    mod.command("Reset Initial.")
    local lines = nvim.nvim_buf_get_lines(buf, 0, pos[1], false)
    for i, line in pairs(lines) do
        mod.command(line)
    end
    --nvim.nvim_err_writeln(inspect(dict))
end

return mod
