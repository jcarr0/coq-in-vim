local nvim = vim.api
local pb = require "prover.pbuffer"

OpenProofBuffer = pb.get_or_create
StartCoq = pb.setup_process
OnCoq = pb.on_coq

ProofBufferMod = pb
