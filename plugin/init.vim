lua require "prover"

command! OpenProofBuffer lua OpenProofBuffer()
command! RunCoq lua StartCoq()
command! Setup :OpenProofBuffer | RunCoq
command! -nargs=* Coq lua ProofBufferMod.command(<q-args>)
command! ReadCoq lua ProofBufferMod.load_fresh_to_cursor()
nnoremap <leader>sr :ReadCoq

command! ProofBufferRespondToCoq lua ProofBufferMod.on_coq_stdout

" so jobstart is stupid right now and nobody's finished the lua api yet
" so we need a vim wrapper that just calls the lua function
function! s:vimLWrapper(job_id, data, event) dict
  call luaeval("OnCoq(_A)", {'job_id': a:job_id, 'data': a:data, 'event': a:event})
endfunction

function! StartCoq(command)
    let job = jobstart(a:command, {
    \ 'on_stdout': function('s:vimLWrapper'),
    \ 'on_stderr': function('s:vimLWrapper'),
    \ 'on_exit': function('s:vimLWrapper'),
    \ })
    return job
endfunction

:OpenProofBuffer
:RunCoq
